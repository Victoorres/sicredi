import LocalStorageContext from '../../contexts/LocalStorageContext';
import { Route, Redirect } from 'react-router-dom';
import React, { useContext } from 'react';

const RoutesPrivate = ({ component: Component, ...rest }: any) => {
  const { token } = useContext(LocalStorageContext);

  return <Route {...rest} render={() => (token ? <Component {...rest} /> : <Redirect to="/login" />)} />;
};

export default RoutesPrivate;
