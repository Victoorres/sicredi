import { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import api from '../../core/services/api';
import '../../styles/Dragons.css';

const defaultDragonImage = 'https://i.pinimg.com/originals/9c/f3/8c/9cf38c6ba47dcb942ac4b38ca956bfb2.jpg';

const dragonDetail = {
  name: '',
  type: '',
  avatar: defaultDragonImage,
};

export default function DragonDetails({ computedMatch }: any) {
  const [image, setImage] = useState(defaultDragonImage);
  const [dragon, setValues] = useState(dragonDetail);
  const dragonId = computedMatch.params.id;
  const history = useHistory();

  useEffect(() => {
    api.get(`/${dragonId}`).then(({ data }: any) => {
      setValues(data);
      setImage(data.avatar);
    });
  }, [dragonId]);

  function changeForm(event: any) {
    const { name, value, type, avatar } = event.target;

    setValues({
      ...dragon,
      [name]: value,
      [type]: value,
      [avatar]: avatar,
    });
  }

  function onSubmit(event: any) {
    event.preventDefault();

    if (dragon.name && dragon.type) {
      api.put(`/${dragonId}`, dragon).then(() => {
        return history.push('/dragons-list');
      });
    }
  }

  function changeImage(event: any): void {
    setImage(event.target.value);
  }

  function handleToBack() {
    history.push('/dragons-list');
  }

  return (
    <div className="body">
      <div className="session">
        <form onSubmit={onSubmit}>
          <div className="create-container">
            <h4>Alterar dragão</h4>
          </div>

          <div className="container" style={{ textAlign: 'center' }}>
            <div className="avatar-upload">
              <div className="avatar-preview">
                <div id="imagePreview" style={{ backgroundImage: `url(${image})` }}></div>
              </div>
            </div>
          </div>

          <div className="group">
            <label>Link do avatar do dragão</label>
            <input
              id="avatar"
              name="avatar"
              type="text"
              value={dragon.avatar}
              onChange={changeForm}
              onKeyUp={changeImage}
              placeholder=""
            />
          </div>

          <div className="group">
            <label>Nome do dragão</label>

            <input id="name" name="name" type="text" value={dragon.name} onChange={changeForm} placeholder="" />
          </div>

          <div className="group">
            <label>Tipo do dragão</label>
            <input id="type" name="type" type="text" placeholder="" value={dragon.type} onChange={changeForm} />
          </div>

          <div className="button-container">
            <button onClick={handleToBack} className="back-button" type="submit">
              Voltar
            </button>

            <button className="change-button" type="submit">
              Alterar
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
