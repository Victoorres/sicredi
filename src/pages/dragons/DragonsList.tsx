import { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import api from '../../core/services/api';
import '../../styles/Dragons.css';

export default function DragonsList() {
  const history = useHistory();
  const [dragons, setDragons] = useState([]);

  useEffect(() => {
    getDragons();
  }, []);

  function getDragons() {
    api
      .get('')
      .then(({ data }) => {
        setDragons(
          (data as any).sort((a: any, b: any) => {
            if (a.name > b.name) return 1;

            if (a.name < b.name) return -1;

            return 0;
          })
        );
      })
      .catch((error) => {
        // alert(error);
      });
  }

  function formatDate(createdAt: any) {
    const date = createdAt.split('T');
    const dateFormated = date[0].split('-');

    return `${dateFormated[2]}/${dateFormated[1]}/${dateFormated[0]}`;
  }

  function handleCreateDragon(): any {
    history.push('/dragon-create');
  }

  function handleEditDragon(dragonId: string): any {
    history.push({ pathname: `/dragon-details/${dragonId}`, state: { id: dragonId } });
  }

  function handleDeleteDragon(dragonId: string): any {
    api.delete(dragonId).finally(() => {
      getDragons();
    });
  }

  return (
    <div className="tableContainer">
      <section>
        <h1>Lista de dragões</h1>
        <div style={{ textAlign: 'right' }}>
          <button className="create-button" onClick={() => handleCreateDragon()}>
            Cadastrar novo dragão
          </button>
        </div>
        <div className="tbl-header">
          <table style={{ overflow: 'auto' }}>
            <thead>
              <tr>
                <th>Avatar</th>
                <th>Nome</th>
                <th>Tipo</th>
                <th>Data de criação</th>
                <th>Ações</th>
              </tr>
            </thead>
          </table>
        </div>
        <div className="tbl-content">
          <table>
            <tbody>
              {dragons.length ? (
                dragons.map((dragon: any) => (
                  <tr key={dragon.id}>
                    <td>
                      {' '}
                      <img src={dragon.avatar} alt="" />
                    </td>
                    <td>{dragon.name}</td>
                    <td>{dragon.type}</td>
                    <td>{formatDate(dragon.createdAt)}</td>
                    <td>
                      <button className="edit-button" onClick={() => handleEditDragon(dragon.id)}>
                        Editar
                      </button>
                      <button className="delete-button" onClick={() => handleDeleteDragon(dragon.id)}>
                        Excluir
                      </button>
                    </td>
                  </tr>
                ))
              ) : (
                <tr>
                  <td className="not-found" colSpan={4}>
                    Nenhum dragão encontrado.
                  </td>
                </tr>
              )}
            </tbody>
          </table>
        </div>
      </section>
    </div>
  );
}
