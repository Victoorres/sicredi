import { useHistory } from 'react-router';
import api from '../../core/services/api';
import '../../styles/Dragons.css';
import { useState } from 'react';

const dragonDefault = {
  name: '',
  type: '',
  avatar: 'https://i.pinimg.com/originals/9c/f3/8c/9cf38c6ba47dcb942ac4b38ca956bfb2.jpg',
};

export default function DragonCreate() {
  const [image, setImage] = useState('https://i.pinimg.com/originals/9c/f3/8c/9cf38c6ba47dcb942ac4b38ca956bfb2.jpg');
  const [dragon, setValues] = useState(dragonDefault);
  const history = useHistory();

  function changeForm(event: any) {
    const { name, value, type, avatar } = event.target;

    setValues({
      ...dragon,
      [name]: value,
      [type]: value,
      [avatar]: value,
    });
  }

  function onSubmit(event: any) {
    event.preventDefault();

    if (dragon.name && dragon.type) {
      api.post('', dragon).then(() => {
        return history.push('/dragons-list');
      });
    }
  }

  function handleToBack() {
    history.push('dragons-list');
  }

  function changeImage(event: any): void {
    setImage(event.target.value);
  }

  return (
    <div className="body">
      <div className="session">
        <form onSubmit={onSubmit}>
          <div className="create-container">
            <h4>Cadastrar dragão</h4>
          </div>

          <div className="container" style={{ textAlign: 'center' }}>
            <div className="avatar-upload">
              <div className="avatar-preview">
                <div id="imagePreview" style={{ backgroundImage: `url(${image})` }}></div>
              </div>
            </div>
          </div>

          <div className="group">
            <label>Link do avatar do dragão</label>
            <input
              id="avatar"
              name="avatar"
              type="text"
              value={dragon.avatar}
              onChange={changeForm}
              onKeyUp={changeImage}
              placeholder=""
            />
          </div>

          <div className="group">
            <label>Nome do dragão</label>

            <input id="name" name="name" type="text" value={dragon.name} onChange={changeForm} placeholder="" />
          </div>

          <div className="group">
            <label>Tipo do dragão</label>
            <input id="type" name="type" type="text" placeholder="" value={dragon.type} onChange={changeForm} />
          </div>

          <div className="button-container">
            <button onClick={handleToBack} className="back-button" type="submit">
              Voltar
            </button>

            <button className="create-button" type="submit">
              Cadastrar
            </button>
          </div>
        </form>
      </div>
    </div>
  );
}
