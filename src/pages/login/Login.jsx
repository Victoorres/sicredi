import LocalStorageContext from '../../contexts/LocalStorageContext';
import generateToken from '../../utils/generateToken';
import { useContext, useState } from 'react';
import { useHistory } from 'react-router';
import './Login.css';

const userDefault = {
  user: '',
  password: '',
};

function login({ user, password }) {
  if (user === 'admin' && password === 'root') {
    return { token: String(generateToken(40)) };
  }
  return {error : 'Usuário ou senha inválida'};
}

export default function Login() {
  const [values, setValues] = useState(userDefault);
  const { setToken } = useContext(LocalStorageContext);
  const history = useHistory();

  function changeForm(event) {
    const { value, name } = event.target;

    setValues({
      ...values,
      [name]: value,
    });
  }

  function onSubmit(event) {
    event.preventDefault();

    const { token } = login(values);

    if (token) {
      setToken(token);
      return history.push('/dragons-list');
    }

    setValues(userDefault);
  }

  return (
    <div className="body">
      <div className="session">
        <form action="" className="log-in" onSubmit={onSubmit}>
          <div className="login-container">
            <h4>Login</h4>
          </div>
          <input placeholder="Usuário" type="text" name="user" id="user" onChange={changeForm} value={values.user} />
          <input placeholder="Senha" type="password" name="password" id="password" onChange={changeForm}  value={values.password} />
          <div className="button-container">
            <button type="submit">Entrar</button>
          </div>
        </form>
      </div>
    </div>
  );
}
