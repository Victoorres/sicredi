import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import LocalStorageProvider from './providers/LocalStorageProvider';
import RoutesPrivate from './routes/private/RoutesPrivate';
import DragonDetails from './pages/dragons/DragonDetails';
import DragonCreate from './pages/dragons/DragonCreate';
import DragonsList from './pages/dragons/DragonsList';
import Login from './pages/login/Login';
import { Redirect } from 'react-router';

function App() {
  return (
    <div className="App">
      <Router>
        <LocalStorageProvider>
          <Switch>
            <Route path="/login" component={Login} />
            <RoutesPrivate path="/dragons-list" component={DragonsList} />
            <RoutesPrivate path="/dragon-create" component={DragonCreate} />
            <RoutesPrivate path="/dragon-details/:id" component={DragonDetails} />
            <Redirect to="/login" />
          </Switch>
        </LocalStorageProvider>
      </Router>
    </div>
  );
}

export default App;
