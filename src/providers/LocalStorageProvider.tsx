import LocalStorageContext from '../contexts/LocalStorageContext';
import useStorage from '../utils/useStorage';

const LocalStorageProvider = ({ children }: any) => {
  const [token, setToken] = useStorage('token');

  return (
    <LocalStorageContext.Provider
      value={{
        token,
        setToken,
      }}
    >
      {children}
    </LocalStorageContext.Provider>
  );
};

export default LocalStorageProvider;
