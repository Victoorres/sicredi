import { createContext } from 'react';

const LocalStorageContext = createContext({
  token: null,
  setToken: () => {},
});

export default LocalStorageContext;
